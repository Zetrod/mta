# Chronicles of Darkness 2e for FoundryVTT (former: Mage the Awakening)

Manifest: https://gitlab.com/MarlQ/mta/-/raw/master/system.json

For contact: SoulCake#7804 in Discord or the wod channel in the FoundryVTT server

If someone wants to help with development, you're always welcome.
I'll continue working on this until I'm done with my current campaign,
though if people are interesting I might continue afterwards.

Dev branch will possibly be unstable, progress branch is for my personal homebrew and the place where most features originate.

If you want to support the project, feel free to [buy me a coffee](https://ko-fi.com/soulcake) :)

Also check out my self-made TAC system (currently on hiatus): https://gitlab.com/MarlQ/tac

## Features

### Images (click for full-size)

[<img src="/screenshots/characterSheet.png" width="168" height="128">](/screenshots/characterSheet.png)
[<img src="/screenshots/improvisedSpellcasting.png" width="128" height="128">](/screenshots/improvisedSpellcasting.png)
[<img src="/screenshots/werewolfSheet.png" width="128" height="128">](/screenshots/werewolfSheet.png)

### Supported Sheets
* Mortals (CofD)
* Mages (MtAw 2e)
* Vampires (VtR 2e)
* Changelings (CtL 2e)
* Werewolf (WtF 2e)
* Sleepwalkers (MtAw 2e)
* Proximi (MtAw 2e)
* Ephemeral Entities (Goetia, Angels, Ghosts, Spirits)

### Current Features

* Sheets for Spells, Active Spells, Attainments, Yantras, Firearms, Melee, Armor, Ammo, Equipment, Services, Merits, Conditions, Tilts, ...
* Tracking of pretty much all the things the original character sheet contains
* Rolling of any combination of Attribute, Skill, and other trait
* Complete dice roller with all rules (e.g. 10-/9-/8-again, rote pools, chance die removing 10-again, and 1-again for rote pools)
* Dice penalties for 0 dots in Skills
* Intelligent health tracking (kudos to Pecklaaz)
* Reloading system from my TAC System (might be a bit over-the-top but it works), in order to reload you have to have ammo your inventory with the same cartridge as the firearm
* Basic Doors/Impression tracking
* Quick calculation of derived stats
* Weapon rolling
* Mage: (Improvised) spell casting! With almost all features as the one on voidstate.com (except Yantras)!
* Beat tracking system (with history, and reasons, etc.)
* Buff any trait via Tilts, Conditions, or Merits
* Combining any dice pool with an item (only certain 'items' like Contracts, Discipline power, etc. for now)

### Currently Working On

See the milestones: https://gitlab.com/MarlQ/mta/-/milestones

Or see the issues list: https://gitlab.com/MarlQ/mta/-/issues (**Report bugs here**)

Sheets for most other CoD splats are planned.

### Localisation

- English
- French by user lowkey

### Credits

* Icons from https://game-icons.net/ (License: https://creativecommons.org/licenses/by/3.0/)
* Icons from fontawesome (License: https://fontawesome.com/license)
* All other icons/textures are CC0
* Some code snippets were used from the dnd 5e system.
* Thanks to Pecklaaz for some of the GUI work.
